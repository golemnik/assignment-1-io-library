%define SYSCALL_EXIT 60
%define SYSCALL_WRITE 1
%define SYSCALL_READ 0
%define STDIN 0
%define STDOUT 1
%define NEWLINE_CHAR 0xA
%define SPACE_CHAR 0x20
%define TAB_CHAR 0x9


section .text

; Принимает код возврата и завершает текущий процесс
; Input
; 	rdi == ret code
; Output
; 	-
exit:
	mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; Input
;	rdi == str ptr*
; Output
; 	rax == length
string_length:
	xor rax, rax
.cycle:
	cmp byte [rdi+rax], 0
	je .return
	inc rax		;inc length
	jmp .cycle
.return:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; Input
; 	rdi == str ptr*
; Output
; 	cout << str
print_string:
	push rdi
	call string_length
	mov rdx, rax
	mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
	pop rsi
	syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
; Input
; 	-
; Output
; 	cout << 0xA
print_newline:
	mov rdi, NEWLINE_CHAR

; Принимает код символа и выводит его в stdout
; Input
; 	rdi == symbol code
; Output
; 	cout << char
print_char:
	push rdi
	mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
	mov rdx, 1 ; size buf
	mov rsi, rsp
	syscall
	pop rdi
	ret


; Выводит знаковое 8-байтовое число в десятичном формате
; Input
; 	rdi == num
; Output
; 	stdout << num
print_int:
    test rdi, rdi
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; Input
; 	rdi == num
; Output
; 	cout <<  (dec) num
print_uint:
    xor rdx,rdx
    mov rcx, 10
.cout_num:
    cmp rdi, rcx
    jae .next
.cout_digit:
    add rdi, '0'
    jmp print_char
.next:
    mov rax, rdi
    div rcx
    mov r10, rax
    mul rcx
    sub rdi, rax
    push di
    mov rdi, r10
    call .cout_num
    pop di
    jmp .cout_digit


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; Input
; 	rdi == str1 ptr*
; 	rsi == str2 ptr*
; Output
; 	rax == ? 1 : 0
string_equals:
.cycle:
    mov al, [rdi]
    mov ah, [rsi]
    cmp al, ah
    jne .neq
    cmp al, 0
    je  .eq
    inc rdi
    inc rsi
    jmp .cycle
.neq:
    xor rax, rax
    ret
.eq:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; Input
; 	cin >> char
; Output
; 	rax == EOF ? 0 : char
read_char:
    sub rsp, 8
	mov rax, SYSCALL_READ ; syscall read
    mov rdi, STDIN ; stdin
	mov rdx, 1 ; size buf
	mov rsi, rsp
	syscall
    cmp rax, 1
    jne .zr
    movzx rax, byte [rsp]
    jmp .end
.zr:
    xor rax, rax
.end:
    add rsp, 8
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; Input
; 	rdi == buf ptr*
; 	rsi == buf_size
; 	cin >> str
; Output
; 	rax == buf_size >= str_size ? buf ptr* : 0
;	rdx == buf_size >= str_size ? str_size : -
read_word:
    push r12
    push r13
    push rsi
    push rdi
    mov r12, rdi
    mov r13, rsi
.skip:
    call read_char
    test rax, rax
    je .eof
    cmp al, SPACE_CHAR
    je .skip
    cmp al, TAB_CHAR
    je .skip
    cmp al, NEWLINE_CHAR
    je .skip
.cycle:
    dec r13
    test r13, r13
    je .of
    mov [r12], al
    inc r12
    call read_char
    test rax, rax
    je .eof
    cmp al, SPACE_CHAR
    je .eow
    cmp al, TAB_CHAR
    je .eow
    cmp al, NEWLINE_CHAR
    je .eow
    jmp .cycle
.eow:
    xor al,al
.eof:
    mov [r12], al
    pop rax
    pop rdx
    sub rdx, r13
    jmp .done
.of:
    pop rax
    xor rax, rax
    pop rdx
.done:
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; Input
; 	rdi == str ptr*
; Output
; 	rax == isDigit ? num : -
;	rdx == isDigit ? str_size : 0
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rsi, rsi
    xor rcx, rcx
    mov r8, 10
.loop:
    mov cl, [rdi]
    cmp cl, 0
    je .done
    cmp cl, '0'
    jb .done
    cmp cl, '9'
    ja .done
    sub cl, '0'
    mul r8
    add rax, rcx
    jo .error
    inc rsi
    inc rdi
    jmp .loop
.error:
    xor rax, rax
    xor rsi, rsi
.done:
    mov rdx, rsi
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; Input
; 	rdi == str ptr*
; Output
; 	rax == isDigit ? num : -
;	rdx == isDigit ? str_size : 0
parse_int:
    mov cl, [rdi]
    cmp cl, '-'
    jne parse_uint
    inc rdi
    call parse_uint
    push rdx
    sar rdx, 7
    test rdx, rdx
    jnz .error
    pop rdx
    test rdx, rdx
    je .done
    inc rdx
    neg rax
.done:
    ret
.error:
    xor rax, rax
    xor rdx, rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; Input
; 	rdi - str ptr*
; 	rsi - buf ptr*
; 	rdx - buf_size
; Output
; 	rax ? len : 0
string_copy:
    push bx
    xor rax, rax
.cycle:
    cmp rax, rdx
    je .of
    inc rax
    mov bl, [rdi]
    mov [rsi], bl
    cmp bl, 0
    je .done
    inc rdi
    inc rsi
    jmp .cycle
.of:
    xor rax, rax
.done:
    pop bx
    ret
